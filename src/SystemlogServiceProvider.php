<?php

namespace Vlinkpackages\Systemlog;

use Illuminate\Support\ServiceProvider;

class SystemlogServiceProvider extends ServiceProvider
{
    public function register()
    {
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('LogBuilder', 'Vlinkpackages\Systemlog\LogBuilder');
    }

     /**
     * creating the main log folder once installing the package.
     *
     */    

    public function boot()
    {
        $path = storage_path().'/logs/logger';
        LogBuilder::createLogDirectories($path);
    }
   
}