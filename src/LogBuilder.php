<?php

namespace Vlinkpackages\Systemlog;

use Illuminate\Support\Facades\File;


class LogBuilder
{
    /**
     * creating the internal log folder once installing the package.
     */
    public static function createLogDirectories($path)
    {
        if(!File::exists($path, 0755, true, true)){
           File::ensureDirectoryExists($path, 0755, true, true);
        }
    }

    /**
     * write log and create log file
     *
     * @param string $logMessage
     *
     */
    public static function logRow($logMessage)
    {
        $types = [
            'Y',
            'F',
            'd'
        ];

        $dateTypes=[];
        foreach($types as $type){
            array_push($dateTypes, date($type));
        }

        $dateTypesForFileName=[];
        foreach($types as $type){
            if($type == 'F') $type = 'm';
            array_push($dateTypesForFileName, date($type));
        }
        

        $logDateDirectories = implode('/', $dateTypes);
        $path = storage_path().'/logs/logger/'. $logDateDirectories.'/';
        
        $logDateFileName = implode('-', $dateTypesForFileName);

        self::createLogDirectories($path);

        $currentStreamedFile = fopen($path.$logDateFileName.'.log', 'a+');
        fwrite($currentStreamedFile, PHP_EOL.PHP_EOL.PHP_EOL.PHP_EOL.$logMessage);
        fclose($currentStreamedFile);
    }


    /**
     * preparing the log message before inserting it.
     */
    public static function set($message)
    {   
        $type = 'Info';
        if(strpos($message, 'ErrorException') !== false){
            $type = 'Error';
        }
        $currentMethod = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        
        $currentMethodUsing = $currentMethod[1]['class'] .'::'.$currentMethod[1]['function'];
        $currentLineOfCode = $currentMethod[0]['line'];
        $date = date('F j, Y, g:i a', time());

        $logMessage = '['. $date .'] '; 
        $logMessage .= $type . ' : ';
        $logMessage .= $message.' ';
        $logMessage .= '('. $currentMethodUsing . ': line '. $currentLineOfCode .').';
        
        self::logRow($logMessage);
    }
   
}